/*
 * sudoku_tools.h
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#ifndef SUDOKU_TOOLS_H_
#define SUDOKU_TOOLS_H_

void free_board_memory(int **board);
int number_of_solutions(int **sudoku_board, int solve);
int get_starting_value_for_solver_at_position(int x, int y, int **sudoku_board);
int valid_number(int x, int y, int **original_board);
void copy_board(int **src, int **dest);
void init_original_board(int **sudoku_board, int **original_board);

#endif /* SUDOKU_TOOLS_H_ */
