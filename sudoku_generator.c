/*
 * sudoku_generator.c
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "sudoku_generator.h"
#include "sudoku_tools.h"

// generates a sudoku board with only one solution
// the board must be deallocated before the reference to it is lost!!!!!
//lower sensitivity means higher chance to get an easy sudoku
int** generate_sudoku(int sensitivity) {
	int **sudoku_board = (int **) malloc(9 * sizeof(int **));
	init_sudoku_board(sudoku_board);
	number_of_solutions(sudoku_board, 1);
	int counter = 0;
	//try to remove numbers while retaining a sudoku with only one solution
	while (counter < sensitivity) {
		int pos = rand() % 81;
		int x = pos % 9;
		int y = pos / 9;
		int num = sudoku_board[x][y];
		if (num == 0) {
			counter++;
		} else {
			sudoku_board[x][y] = 0;
			if (number_of_solutions(sudoku_board, 0) > 1) {
				sudoku_board[x][y] = num;
				counter++;
			}
		}
	}
	return sudoku_board;
}

void init_sudoku_board(int **sudoku_board) {
	for (int i = 0; i < 9; i++) {
		int *row = (int *) malloc(9 * sizeof(int *));
		for (int j = 0; j < 9; j++) {
			row[j] = 0;
		}
		sudoku_board[i] = row;
	}

	srand(time(NULL));
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			if ((i < 3 && j < 3) || (i > 2 && i < 6 && j > 2 && j < 6)
					|| (i > 5 && j > 5)) {
				int r = rand() % 9 + 1;
				sudoku_board[i][j] = r;
				while (!valid_number(i, j, sudoku_board)) {
					int r = rand() % 9 + 1;
					sudoku_board[i][j] = r;
				}
			}
		}
	}
}
