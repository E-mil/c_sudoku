/*
 * sudoku.c
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#include <stdio.h>
#include <stdlib.h>
#include "sudoku.h"
#include "sudoku_generator.h"
#include "sudoku_tools.h"

int main() {
	printf("SUdOKU GAME!\n");
	game_loop();
	return 0;
}

void game_loop() {
	int game_running = 1;
	while (game_running) {
		int sensitivity = 0;
		char sensitivity_s[10];
		printf(
				"\nEnter the desired difficulty as an integer between 1 and 1000, "
						"with 1 being very easy and 1000 very difficult.\n"
						"The bigger the number the longer it takes to generate the sudoku. 30 is default.\n> ");
		fgets(sensitivity_s, 10, stdin);
		sensitivity = atoi(sensitivity_s);
		if (sensitivity < 1 || sensitivity > 1000) {
			printf("Invalid number, default difficulty will be used.\n");
			sensitivity = 30;
		}
		int **sudoku_board = generate_sudoku(sensitivity);
		int **original_board = (int **) malloc(9 * sizeof(int **));
		init_original_board(sudoku_board, original_board);
		int command = 0;
		print_board(sudoku_board);
		int sudoku_session_running = 1;
		while (sudoku_session_running) {
			print_menu(sudoku_board);
			char command_s[10];
			fgets(command_s, 10, stdin);
			command = atoi(command_s);
			switch (command) {
			case 1:
				printf("Insert number mode:\n");
				print_board(sudoku_board);
				printf(
						"Type a new command after the \">\".\n"
								"Commands should be in the format row_number column_number number_to_insert. Extra numbers will be ignored.\n"
								"number_to_insert can be set to 0 to erase an inserted number.\n"
								"Examples:\n\t> 1 1 4\n\t> 114\n\t> 1145\nThese commands are treated the same and they insert "
								"the number 4 in the upper left corner of the board.\n"
								"Type 0 to quit.\n");
				char input_s[15];
				while (1) {
					printf("> ");
					fgets(input_s, 15, stdin);
					if (input_s[0] == '0') {
						break;
					}
					char x_s, y_s, num_s;
					sscanf(input_s, "%c %c %c", &x_s, &y_s, &num_s);
					insert_number(--x_s - 48, --y_s - 48, num_s - 48,
							sudoku_board, original_board);
				}
				break;
			case 2:
				if (number_of_solutions(sudoku_board, 0) > 0) {
					printf("\nThere are no errors\n");
				} else {
					printf("\nThere is an error somewhere\n");
				}
				break;
			case 3:
				print_board(sudoku_board);
				break;
			case 4:
				printf("\nCreating new sudoku...\n");
				sudoku_session_running = 0;
				break;
			case 5:
				printf(
						"\nSudoku is a logic-based number-puzzle.\nEach row, column, and box in the grid must contain the numbers"
								" 1-9 when the sudoku is finished.\n"
								"There must be no duplicates of numbers in the rows, columns, or boxes.\n"
								"You can not change a number that was part of the original sudoku board.\n");
				break;
			case 6:
				printf("\nHejdå!\n");
				sudoku_session_running = 0;
				game_running = 0;
				break;
			default:
				printf(
						"Unknown command, please try again with a valid number\n");
			}
		}
		free_board_memory(sudoku_board);
		free_board_memory(original_board);
	}
}

void insert_number(int x, int y, int num, int **sudoku_board,
		int **original_board) {
	if (x < 0 || x > 8 || y < 0 || y > 8 || num < 0 || num > 9) {
		printf("Wrong input format, please try again\n");
	} else {
		if (original_board[x][y] != 0) {
			printf(
					"You are not allowed to change this number, please enter a different command.\n");
		} else {
			sudoku_board[x][y] = num;
			print_board(sudoku_board);
		}
	}
}

void print_menu(int **sudoku_board) {
	printf("\n");
	printf(
			"1: Insert number mode\n2: Check correctness\n3: View board\n4: New sudoku\n5: Info\n6: Exit\n");
	printf("> ");
}

// prints a nicely formatted sudoku board in the terminal
void print_board(int **sudoku_board) {
	printf("\n\t");
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			if (sudoku_board[i][j] == 0) {
				printf("_ ");
			} else {
				printf("%d ", sudoku_board[i][j]);
			}
			if ((j + 1) % 3 == 0 && j != 8) {
				printf("| ");
			}
		}
		printf("\n\t");
		if ((i + 1) % 3 == 0 && i != 8) {
			printf("---------------------");
			printf("\n\t");
		}
	}
	printf("\n");
}
