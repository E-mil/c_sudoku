/*
 * sudoku_tools.c
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sudoku_tools.h"

void free_board_memory(int **board) {
	for (int i = 0; i < 9; i++) {
		free(board[i]);
	}
	free(board);
}

// if solve == 0; returns:
// 0 if there are no solutions
// 1 if there is one solution
// 2 if there are more than one solutions
// if solve == 1; returns:
// 0 if there are no solutions
// 1 if there is one solution. Also, the board is not reset to the input board.
int number_of_solutions(int **sudoku_board, int solve) {
	int number_of_solutions = 0;
	int **original_board = (int **) malloc(9 * sizeof(int **));

	init_original_board(sudoku_board, original_board);

	int direction = 1;

	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			int starting_value = get_starting_value_for_solver_at_position(i, j, original_board);
			//check if at editable spot; otherwise, move to next spot in direction
			if (original_board[i][j] == 0) {
				//if empty spot, add 9; if no more numbers, go back one step; else, decrement and find valid number
				if (sudoku_board[i][j] == starting_value % 9 + 1) {
					sudoku_board[i][j] = 0;
					//////////////////////
					//MOVING BACKWARDS
					j -= 2;
					if (j < -1) {
						i--;
						j = 7;
						if (i < 0) {
							copy_board(original_board, sudoku_board);
							free_board_memory(original_board);
							return number_of_solutions;
						}
					}
					direction = -1;
					//////////////////////
				} else {
					if (sudoku_board[i][j] == 0) {
						sudoku_board[i][j] = starting_value;
					} else {
						sudoku_board[i][j]--;
						if (sudoku_board[i][j] == 0) {
							sudoku_board[i][j] = 9;
						}
					}
					//decrement number until it is valid
					while (1) {
						if (valid_number(i, j, sudoku_board)) {
							direction = 1;
							break;
						} else {
							sudoku_board[i][j]--;
							if (sudoku_board[i][j] == 0) {
								sudoku_board[i][j] = 9;
							}
							if (sudoku_board[i][j] == starting_value) {
								sudoku_board[i][j] = 0;
								//////////////////////
								//MOVING BACKWARDS
								j -= 2;
								if (j < -1) {
									i--;
									j = 7;
									if (i < 0) {
										copy_board(original_board,
												sudoku_board);
										free_board_memory(original_board);
										return number_of_solutions;
									}
								}
								direction = -1;
								//////////////////////
								break;
							}
						}
					}
				}
			} else {
				if (!valid_number(i, j, sudoku_board)) {
					copy_board(original_board, sudoku_board);
					free_board_memory(original_board);
					return number_of_solutions;
				}
				if (direction < 0) {
					//////////////////////
					//MOVING BACKWARDS
					j -= 2;
					if (j < -1) {
						i--;
						j = 7;
						if (i < 0) {
							copy_board(original_board, sudoku_board);
							free_board_memory(original_board);
							return number_of_solutions;
						}
					}
					//////////////////////
				}
			}
			//if the end of the sudoku board has been reached
			if (i == 8 && j == 8) {
				number_of_solutions++;
				if (number_of_solutions > 1) {
					copy_board(original_board, sudoku_board);
					free_board_memory(original_board);
					return number_of_solutions;
				}
				if (solve > 0) {
					free_board_memory(original_board);
					return number_of_solutions;
				}
				//////////////////////
				//MOVING BACKWARDS
				j -= 2;
				if (j < -1) {
					i--;
					j = 7;
					if (i < 0) {
						copy_board(original_board, sudoku_board);
						free_board_memory(original_board);
						return number_of_solutions;
					}
				}
				direction = -1;
				//////////////////////
			}
		}
	}

	// this code should never be executed
	printf("Something went wrong while generating a new sudoku\n");
	copy_board(original_board, sudoku_board);
	free_board_memory(original_board);
	return 0;
}

int get_starting_value_for_solver_at_position(int x, int y,
		int **original_board) {
	return ((x * y + original_board[1][1] * original_board[4][4])
			% (original_board[7][7] + 1) + x + y) % 9 + 1;
}

// returns:
// 1 if valid
// 0 if invalid
int valid_number(int x, int y, int **sudoku_board) {
	int num = sudoku_board[x][y];
	for (int i = 0; i < 9; i++) {
		// x row
		if (i != x && sudoku_board[i][y] == num) {
			return 0;
		}
		// y row
		if (i != y && sudoku_board[x][i] == num) {
			return 0;
		}
	}
	// box
	for (int i = (x / 3) * 3; i < (x / 3) * 3 + 3; i++) {
		for (int j = (y / 3) * 3; j < (y / 3) * 3 + 3; j++) {
			if ((x != i || y != j) && sudoku_board[i][j] == num) {
				return 0;
			}
		}
	}
	// no collision
	return 1;
}

// copies the contents of src to dest
void copy_board(int **src, int **dest) {
	for (int i = 0; i < 9; i++) {
		memcpy(dest[i], src[i], 9 * sizeof(int *));
	}
}

// allocate memory for the rows in temp_board and set them to the same values as sudoku_board
void init_original_board(int **sudoku_board, int **original_board) {
	for (int i = 0; i < 9; i++) {
		int *row = malloc(9 * sizeof(int *));
		memcpy(row, sudoku_board[i], 9 * sizeof(int *));
		original_board[i] = row;
	}
}
