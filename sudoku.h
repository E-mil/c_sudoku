/*
 * sudoku.h
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#ifndef SUDOKU_H_
#define SUDOKU_H_

void game_loop();
void print_menu(int **sudoku_board);
void insert_number(int x, int y, int num, int **sudoku_board, int **original_board);
void print_board(int **sudoku_board);

#endif /* SUDOKU_H_ */
