/*
 * sudoku_generator.h
 *
 *  Created on: Mar 3, 2018
 *      Author: emil
 */

#ifndef SUDOKU_GENERATOR_H_
#define SUDOKU_GENERATOR_H_

int** generate_sudoku(int sensitivity);
void init_sudoku_board(int **sudoku_board);

#endif /* SUDOKU_GENERATOR_H_ */
