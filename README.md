# C_Sudoku

A simple command based sudoku game, written in C with the purpose of learning more C.

## Example of what the program looks like:

	SUdOKU GAME!

	Enter the desired difficulty as an integer between 1 and 1000, with 1 being very easy and 1000 very difficult.
	The bigger the number the longer it takes to generate the sudoku. 30 is default.
	> 100

		_ _ _ | 9 6 _ | _ _ 8 
		_ 3 _ | _ _ _ | _ 9 _ 
		9 _ _ | _ 1 _ | _ _ 7 
		---------------------
		5 _ _ | _ _ _ | _ 3 _ 
		8 _ _ | 5 _ 2 | 1 _ _ 
		_ _ 1 | _ _ _ | _ _ _ 
		---------------------
		_ _ _ | _ 4 _ | 5 _ 1 
		3 _ 7 | 1 2 5 | _ _ _ 
		4 _ _ | _ _ 8 | _ _ _ 
	

	1: Insert number mode
	2: Check correctness
	3: View board
	4: New sudoku
	5: Info
	6: Exit
	> 1
	Insert number mode:

		_ _ _ | 9 6 _ | _ _ 8 
		_ 3 _ | _ _ _ | _ 9 _ 
		9 _ _ | _ 1 _ | _ _ 7 
		---------------------
		5 _ _ | _ _ _ | _ 3 _ 
		8 _ _ | 5 _ 2 | 1 _ _ 
		_ _ 1 | _ _ _ | _ _ _ 
		---------------------
		_ _ _ | _ 4 _ | 5 _ 1 
		3 _ 7 | 1 2 5 | _ _ _ 
		4 _ _ | _ _ 8 | _ _ _ 
	
	Type a new command after the ">".
	Commands should be in the format row_number column_number number_to_insert. Extra numbers will be ignored.
	number_to_insert can be set to 0 to erase an inserted number.
	Examples:
		> 1 1 4
		> 114
		> 1145
	These commands are treated the same and they insert the number 4 in the upper left corner of the board.
	Type 0 to quit.
	> 461
	
		_ _ _ | 9 6 _ | _ _ 8 
		_ 3 _ | _ _ _ | _ 9 _ 
		9 _ _ | _ 1 _ | _ _ 7 
		---------------------
		5 _ _ | _ _ 1 | _ 3 _ 
		8 _ _ | 5 _ 2 | 1 _ _ 
		_ _ 1 | _ _ _ | _ _ _ 
		---------------------
		_ _ _ | _ 4 _ | 5 _ 1 
		3 _ 7 | 1 2 5 | _ _ _ 
		4 _ _ | _ _ 8 | _ _ _ 
		
	> 921

		_ _ _ | 9 6 _ | _ _ 8 
		_ 3 _ | _ _ _ | _ 9 _ 
		9 _ _ | _ 1 _ | _ _ 7 
		---------------------
		5 _ _ | _ _ 1 | _ 3 _ 
		8 _ _ | 5 _ 2 | 1 _ _ 
		_ _ 1 | _ _ _ | _ _ _ 
		---------------------
		_ _ _ | _ 4 _ | 5 _ 1 
		3 _ 7 | 1 2 5 | _ _ _ 
		4 1 _ | _ _ 8 | _ _ _ 
	
	> 533
	
		_ _ _ | 9 6 _ | _ _ 8 
		_ 3 _ | _ _ _ | _ 9 _ 
		9 _ _ | _ 1 _ | _ _ 7 
		---------------------
		5 _ _ | _ _ 1 | _ 3 _ 
		8 _ 3 | 5 _ 2 | 1 _ _ 
		_ _ 1 | _ _ _ | _ _ _ 
		---------------------
		_ _ _ | _ 4 _ | 5 _ 1 
		3 _ 7 | 1 2 5 | _ _ _ 
		4 1 _ | _ _ 8 | _ _ _ 
	
	> 0

	1: Insert number mode
	2: Check correctness
	3: View board
	4: New sudoku
	5: Info
	6: Exit
	>